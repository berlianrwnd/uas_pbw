const container = document.querySelector('.container');
const seats = document.querySelectorAll('.row .seat:not(.Terisi)');
const count = document.getElementById('count');
const total = document.getElementById('total');
const film = document.getElementById('film');

populateUI();
let ticketprice = +film.value;

function setFilmData(filmIndex, filmPrice){
	localStorage.setItem('selectedFilmIndex', filmIndex);
	localStorage.setItem('selectedFilmPrice', filmIndex);S
}

function updateSelectedCount(){
	const selectedSeats = document.querySelectorAll('.row .seat .selected');

	const seatsIndex = [...selectedSeats].map(function(seat){
		return [...seats].indexOf(seat);
	});

	localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex))

	const selectedSeatsCount = selectedSeats.length;

	count.innerText = selectedSeatsCount;
	total.innerText = selectedSeatsCount * ticketprice;
}

function populateUI(){
	const selectedSeats = JSON.parse(localStorage.getItems('selectedSeats'));

	if(selectedSeats !==null && selectedSeats.length > 0){
		seats.forEach((seat, index)=> {
			if (selectedSeats.indexOf(index) > - 1) {
				seat.classList.add('selected');
			}
		});
	}
const selectedFilmIndex = localStorage.getItem('selectedFilmIndex');

if (selectedFilmIndex != null){
	filmSelect.selectedIndex = selectedIndex;
}
}

film.addEventListener ('change', e => {
	ticketprice = +e.target.value;
	setFilmData(e.target.selectedIndex, e.target.value);
	updateSelectedCount();
});

container.addEventListener('click', (e)=> {
	if(e.target.classList.contains('seat') && !e.target.classList.contains('Terisi')) {
		e.target.classList.toggle('Dipilih');
	}

	updateSelectedCount();
});